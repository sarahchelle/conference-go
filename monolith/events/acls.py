from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = "https://api.pexels.com/v1/search"
    params = {"query": f"{city}, {state}, cityscape", "per_page": 1}
    res = requests.get(url, params=params, headers=headers)
    result = res.json()
    picture_dict = {"picture_url": result["photos"][0]["src"]["medium"]}
    return picture_dict


def get_weather(city, state):
    geo_url = "http://api.openweathermap.org/geo/1.0/direct"
    geo_params = {"appid": OPEN_WEATHER_API_KEY, "q": f"{city},{state},USA"}
    geo_res = requests.get(geo_url, params=geo_params)
    geo_result = geo_res.json()
    lat, lon = (geo_result[0]["lat"], geo_result[0]["lon"])
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "appid": OPEN_WEATHER_API_KEY,
        "lat": lat,
        "lon": lon,
        "units": "imperial",
    }
    res = requests.get(url, params=params)
    result = res.json()
    temp = result["main"]["temp"]
    description = result["weather"][0]["description"]
    weather_dict = {"weather": {"temp": temp, "description": description}}
    return weather_dict
